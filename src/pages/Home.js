import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import {Button} from 'react-bootstrap';


export default function Home() {
    const heading = "Zuitt Coding Bootcamp"
    const text = "Opportunities for everyone, everywhere.";

	return (
		<>
		<Banner heading={heading} text={text}>
            <p>{text}</p>
            <Button variant="primary">Enroll now!</Button>
        </Banner>
    	<Highlights />
		</>
	)
}