import Banner from '../components/Banner';
import { useNavigate } from 'react-router-dom';
import '../styles/pointer.css';


export default function Error404() {

    const navigate = useNavigate();
    const heading = "Page Not Found";

    function handleClick(e) {
        navigate('/');
    }

    return (
        <Banner heading={heading}>
            <p>Go back to  
                <span className="text-primary hover-pointer" 
                onClick={handleClick}
                > homepage
                </span>.
            </p>
        </Banner>
    )
}