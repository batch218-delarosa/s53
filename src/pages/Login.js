import {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

export default function Login(props) {

    const [email, setEmail] = useState("");
    const [pswd, setPswd] = useState("");

    const [isActive, setIsActive] = useState(false);

    // hook returns a function that lets us navigate to components
    const navigate = useNavigate();

    function authenticate(e) {
        e.preventDefault();

        localStorage.setItem('email', email);

        setEmail("");
        setPswd("");
        navigate('/');

        alert(`${email} has been authenticated. Thank you for logging in.`);
        console.log("Logged in");
        window.location.reload();


    }

    useEffect(() => {

        if (pswd === "" || email.length === "") {
            setIsActive(false);
            return;
        }

        setIsActive(true);



    },[email, pswd])


    return (
        <>
        <h1>Login</h1>
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group  controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                required
                    value={email}
                    onChange={e => {
                        setEmail(e.target.value);
                    }}
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                required
                    value={pswd}
                    onChange={e => {
                        setPswd(e.target.value);
                    }}
                />
            </Form.Group>

            <Button className="mt-3" variant="success" type="submit" id="submitBtn" 
            disabled={!isActive}
            >
            	Submit
            </Button>
        </Form>

        </>
    )

}