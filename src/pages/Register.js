import {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';


export default function Register() {

    const [email, setEmail] = useState("");
    const [pswd, setPswd] = useState("");
    const [verifyPswd, setVerifyPswd] = useState("");

    const [isActive, setIsActive] = useState(false);

    function ValidateEmail(email) {

        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        if (email.match(validRegex)) {
      
          return true;
      
        } else {
      
          return false;
      
        }
      
      }

    function registerUser(e) {
        e.preventDefault();
        setEmail("");
        setPswd("");
        setVerifyPswd("");

        alert("Thank you for registering");
        console.log("registered");


    }

    useEffect(() => {

        if (pswd.length <= 0 || verifyPswd.length <= 0 || email.length <= 0) {
            return;
        }

        if (pswd === verifyPswd && ValidateEmail(email) === true) {
            setIsActive(true);
            return
        }
        
        setIsActive(false);

    },[email, pswd, verifyPswd])


    return (
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group  controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                required
                    value={email}
                    onChange={e => {
                        setEmail(e.target.value);
                    }}
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                required
                    value={pswd}
                    onChange={e => {
                        setPswd(e.target.value);
                    }}
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                required
                    value={verifyPswd}
                    onChange={e => {
                        setVerifyPswd(e.target.value);
                    }}
                />
            </Form.Group>

            <Button variant={isActive ? 'primary' : 'danger'} type="submit" id="submitBtn" 
            disabled={!isActive}
            >
            	Submit
            </Button>
        </Form>
    )

}