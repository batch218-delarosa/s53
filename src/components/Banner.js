// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Button, Row, Col } from 'react-bootstrap';

export default function Banner(props) {
return (
    <Row>
    	<Col className="p-5">
            <h1>{props.heading}</h1>
            {props.children}
        </Col>
    </Row>
	)
}